﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ksl_service
{
    public static class Library
    {
        public static void WriteErrorLog(Exception ex)
        {
            try
            {
                StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"LogFile.txt", true);
                sw.WriteLine("[" + DateTime.Now + "] -- " + ex.Source.ToString().Trim() + ": " + ex.Message.ToString().Trim());
                sw.Flush();
                sw.Close();
            }
            catch { }
        }

        public static void WriteErrorLog(string message)
        {
            try
            {
                StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"LogFile.txt", true);
                sw.WriteLine("[" + DateTime.Now + "] -- " + message.Trim());
                sw.Flush();
                sw.Close();
            }
            catch { }
        }
    }
}
