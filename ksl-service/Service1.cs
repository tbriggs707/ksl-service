﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Timers;
using System.IO;

namespace ksl_service
{
    public partial class KSL_Service : ServiceBase
    {
        private System.Timers.Timer timer;

        public KSL_Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer = new System.Timers.Timer();
            timer.Interval = 60000; // 900000 = 15 min, 60000 = 1 min
            timer.Elapsed += new ElapsedEventHandler(RunWatcher);
            timer.Enabled = true;
            Library.WriteErrorLog("Service has started...\n");
        }

        protected override void OnStop()
        {
            timer.Enabled = false;
            Library.WriteErrorLog("\n\nService has stopped...");
        }

        private void LogStuff(object sender, ElapsedEventArgs e)
        {
            Library.WriteErrorLog("LogStuff every 30 seconds.");
        }

        private void RunWatcher(object sender, ElapsedEventArgs e)
        {
            string path = KSL_Watcher.Program.GetWorkingDir();
            Process process = new Process();

            // Configure the process using the StartInfo properties.
            process.StartInfo.FileName = path + @"\KSL-Watcher.exe";
            process.StartInfo.Arguments = "-h";
            process.Start();
            process.WaitForExit();
        }


    }
}
